import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wifi/wifi.dart';
import 'package:wifi_iot/wifi_iot.dart';
import 'package:wifi_configuration/wifi_configuration.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Wifi',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _wifiName = 'click button to get wifi ssid.';
  int level = 0, status=1;
  String _ip = 'click button to get ip.';
  List<WifiResult> ssidList = [];
  String ssid = '', password = '', connected_wifi = '';
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    WiFiForIoTPlugin.setEnabled(true);
    loadData();
  }

  Future<void> _onRefresh() async {
    // monitor network fetch
    loadData();
    await Future.delayed(Duration(milliseconds: 1500));
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              controller: _refreshController,
              onRefresh: _onRefresh,
              child: ListView.builder(
                padding: EdgeInsets.all(8.0),
                itemCount: ssidList.length,
                itemBuilder: (BuildContext context, int index) {
                  return itemSSID(index);
                },
              ),
            ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              if(status==1){
                status=0;
                WiFiForIoTPlugin.setEnabled(false);
              }
              else{
                status=1;
                WiFiForIoTPlugin.setEnabled(true);
              }
            });
            // Add your onPressed code here!
          },
          child: status!=1 ?Icon(Icons.network_wifi):Icon(Icons.signal_wifi_off),
          backgroundColor: Colors.blue,
        ),
      ),
    );
  }

  Widget itemSSID(index) {
    if (ssidList[index].ssid == connected_wifi) {
      return GestureDetector(
        child: Column(children: <Widget>[
          ListTile(
            leading: Icon(Icons.wifi, size: 25, color: Colors.blueAccent),
            title: Text(
              ssidList[index].ssid,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 16.0,
                fontFamily: '_champagne_limousines_thick',
              ),
            ),
            subtitle: Text(
              "Connected",
              style: TextStyle(
                color: Colors.grey,
                fontSize: 12.0,
                fontFamily: '_champagne_limousines_thick',
              ),
            ),
          ),
        ]),
      );
    } else {
      return GestureDetector(
        child: Column(children: <Widget>[
          ExpansionTile(
            leading: ssidList[index].level == 3
                ? Icon(Icons.wifi, size: 25, color: Colors.black)
                : ssidList[index].level == 2
                    ? Icon(Icons.wifi, size: 25, color: Colors.black87)
                    : ssidList[index].level == 1
                        ? Icon(Icons.wifi, size: 25, color: Colors.black38)
                        : Icon(Icons.wifi, size: 25, color: Colors.black12),
            title: Text(
              ssidList[index].ssid,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 16.0,
                fontFamily: '_champagne_limousines_thick',
              ),
            ),
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: TextField(
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        password = value;
                        ssid = ssidList[index].ssid;
                      },
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 15),
                          border: InputBorder.none,
                          hintText: 'Enter the pass code'),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: RaisedButton(
                      child: Text(
                        'Connection',
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16.0,
                          fontFamily: '_champagne_limousines_thickbold',
                        ),
                      ),
                      onPressed: () async {
                        WifiConnectionStatus connectionStatus =
                            await WifiConfiguration.connectToWifi(ssid,
                                password, "com.hellodati.wifimodule.host");
                        print("is Connected : ${connectionStatus}");
                        connection(connectionStatus.toString());
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ]),
      );
    }
  }

  void loadData() async {
    WifiConfiguration.connectedToWifi();
    String wifiName = await Wifi.ssid;
    Wifi.list("").then((list) {
      List<WifiResult> list1 = [];
      setState(() {
        for (int i = 0; i < list.length; i++) {
          if (list.elementAt(i).ssid == wifiName)
            list1.add(list.elementAt(i));
        }
        for (int i = 0; i < list.length; i++) {
          if (list.elementAt(i).ssid != "" && list.elementAt(i).ssid != wifiName)
            list1.add(list.elementAt(i));
        }
        for(WifiResult w in list1){
          list1=_removeRedundantElement(list1,w);
        }
        ssidList = list1;
        connected_wifi = wifiName;
      });
    });
  }

  bool _checkExistOneTime(List<WifiResult> wifiList, WifiResult wifi){
    int i=0;
    for(WifiResult w in wifiList){
      if(w.ssid==wifi.ssid){
        i++;
      }
    }
    if(i==1){
      return true;
    }
    else{
      return false;
    }
  }

  List<WifiResult> _removeRedundantElement(List<WifiResult> wifiList, WifiResult wifi){
    if(!_checkExistOneTime(wifiList, wifi)){
      int i=0;
      for(WifiResult w in wifiList){
        if(w.ssid==wifi.ssid){
          if(i==0){
            i++;
          }
          else{
            wifiList.remove(w);
          }
        }
      }
      return wifiList;
    }
    else{
      return wifiList;
    }
  }

  Future<Null> _getWifiName() async {
    int l = await Wifi.level;
    String wifiName = await Wifi.ssid;
    setState(() {
      level = l;
      _wifiName = wifiName;
    });
  }

  Future<Null> _getIP() async {
    String ip = await Wifi.ip;
    setState(() {
      _ip = ip;
    });
  }

  Future<void> connection(String status) async {
    WifiState wifiState = await Wifi.connection(ssid, password);
    if (status == WifiConnectionStatus.connected.toString()) {
      Fluttertoast.showToast(
          msg: "Connected Successfully!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.lightBlue,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "Wrong password, Check it!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0);
    }
    setState(() {
      password = "";
      loadData();
    });
  }

  Future<void> getAPStatus() async {
    int ss= await WiFiForIoTPlugin.getWiFiAPState();
    print(ss);
  }
}

