//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"

#if __has_include(<fluttertoast/FluttertoastPlugin.h>)
#import <fluttertoast/FluttertoastPlugin.h>
#else
@import fluttertoast;
#endif

#if __has_include(<wifi/WifiPlugin.h>)
#import <wifi/WifiPlugin.h>
#else
@import wifi;
#endif

#if __has_include(<wifi_configuration/WifiConfigurationPlugin.h>)
#import <wifi_configuration/WifiConfigurationPlugin.h>
#else
@import wifi_configuration;
#endif

#if __has_include(<wifi_iot/WifiIotPlugin.h>)
#import <wifi_iot/WifiIotPlugin.h>
#else
@import wifi_iot;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [FluttertoastPlugin registerWithRegistrar:[registry registrarForPlugin:@"FluttertoastPlugin"]];
  [WifiPlugin registerWithRegistrar:[registry registrarForPlugin:@"WifiPlugin"]];
  [WifiConfigurationPlugin registerWithRegistrar:[registry registrarForPlugin:@"WifiConfigurationPlugin"]];
  [WifiIotPlugin registerWithRegistrar:[registry registrarForPlugin:@"WifiIotPlugin"]];
}

@end
